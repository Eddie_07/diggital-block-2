using System.Collections;

using System.Collections.Generic;

using UnityEngine;



public class ExampleScript : MonoBehaviour
{
    
/*
     Assignment Operators:

     =
 
    += speed += modifierSpeed; speed = speed + modifierSpeed; speed = 10.0f + 5.0f;
     
     -= speed -= modifierSpeed; speed = speed - modifierSpeed; speed = 10.0f - 5.0f; 
     
     *= speed *= modifiedSpeed; speed = speed * modifiedSpeed;
 
    /=

     %=
 

    Arithmetic Operators:

    +

    -

    *

    /

    % 7%3 = 1

    ++ (+1)

    -- (-1)


    Comparison Operators:

    == Is equal to
 
    != Not equal to

    >

    <

    >=

    <=

 
   Logical Operators:

    && And

    || Or
 
   ! Not
 
   */


    public float speed = 0.0f;

    public float distance = 0.0f;

    public float time = 0.0f;


	// Use this for initialization

	void Start ()
    {
        speed = distance / time;

        if(speed > 70 || speed < 40)
        {
            print("You are breaking the law!");
        }

        print("You are traveling at " + speed + " MPH.");

	}

	// Update is called once per frame

	void Update ()
    {


    }

}
